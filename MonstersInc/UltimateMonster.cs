﻿namespace MonstersInc
{
    internal class UltimateMonster : Monster, IBittingMonster, IFlyingMonster, IMonsterWithWeapons
    {
        public int Bite()
        {
            System.Console.WriteLine("Super duper ultimate bite");
            return 4;
        }

        public void Fly()
        {
            DefaultFlyingBehavior();
        }

        public int UseWeapon()
        {
            System.Console.WriteLine("Ma greatsword will defeat all poor heroes");
            return 5;
        }
    }
}