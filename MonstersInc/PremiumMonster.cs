﻿using System;

namespace MonstersInc
{
    public class PremiumMonster : Monster, IMonsterWithWeapons, IFlyingMonster
    {
        public void Fly()
        {
            DefaultFlyingBehavior();
        }

        public int UseWeapon()
        {
            Console.WriteLine("Monster spear attack!!!");
            return 4;
        }
    }
}
