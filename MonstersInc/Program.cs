﻿using System;

namespace MonstersInc
{
    class Program
    {
        // The type of monsters is defined by an interfaces 
        // which allow to define all diferent behaviors for every monster type.
        // Protected DefaultFlyingBehavior was added to Monster abstract class so we 
        // don't require any duplicate code. We could use stategy pattern here - 
        // but i think for this case it's overengineering.
        static void Main(string[] args)
        {
            Monster monster = new UltimateMonster();
            IBittingMonster bittingMonster = new UltimateMonster();

            UltimateMonster ultimateMonster = new UltimateMonster();
            //perform ultimate attack
            ultimateMonster.Fly();
            ultimateMonster.UseWeapon();
            ultimateMonster.Bite();

            Console.ReadKey();
        }
    }
}
