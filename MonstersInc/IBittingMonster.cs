﻿namespace MonstersInc
{
    internal interface IBittingMonster
    {
        int Bite();
    }
}