﻿using System;

namespace MonstersInc
{
    public class AverageMonster : Monster, IBittingMonster
    {
        public int Bite()
        {
            Console.WriteLine("Average bite, grhhh!");
            return 3;
        }
    }
}
