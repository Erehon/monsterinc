﻿namespace MonstersInc
{
    internal interface IMonsterWithWeapons
    {
        int UseWeapon();
    }
}