﻿namespace MonstersInc
{
    internal interface IFlyingMonster
    {
        void Fly();
    }
}