﻿using System;

namespace MonstersInc
{
    public abstract class Monster
    {
        public string Name { get; set; }

        public void BeScary()
        {
            Console.WriteLine("Roaaaarrr");
        }

        protected void DefaultFlyingBehavior()
        {
            Console.WriteLine("I'm not a superman nor plane. I'm flying monster instead!");
        }
    }
}
